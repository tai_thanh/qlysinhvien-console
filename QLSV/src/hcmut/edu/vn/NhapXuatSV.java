package hcmut.edu.vn;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class NhapXuatSV {
	static ArrayList<SinhVien> arr=new ArrayList<SinhVien>();
	public void AddSV(){
		Scanner sc1=new Scanner(System.in);
		System.out.println("Nhập vào Tên sinh viên ");
		SinhVien sv =new SinhVien();
		sv.setName(sc1.nextLine());
		System.out.println("Nhập vào Địa Chỉ sinh viên ");
		sv.setAddress(sc1.nextLine());
		arr.add(sv);
	}
	public void ghiFile(){
		try {
			File f = new File("D:/textVT1.txt");
			FileOutputStream fo;
			ObjectOutputStream oStream = new ObjectOutputStream(new FileOutputStream(f,true)) {
				protected void writeStreamHeader() throws IOException {
					reset();
				}
			};
			oStream.writeObject(arr.get(arr.size()-1).toString());
			System.out.println("Ghi File thành công");
			oStream.close();
		} catch (IOException e) {
			System.out.println("Không Thành Công");
		}
	}
	public void DeleteFile(){
		Path p1=Paths.get("D:/textVT1.txt");
		try {
			Files.delete(p1);
			System.out.println("Xóa File Thành Công");
		} catch (IOException e) {
			System.out.println("File đã bị xóa hoặc không tồn tại");
		}
	}
	public void ReadFile(){
		String s="";
		try {
			BufferedReader read=new BufferedReader(new FileReader("D:/textVT1.txt"));
			String chuoi;
			while((chuoi=read.readLine())!=null)
				s+=chuoi;
			System.out.println("File được đọc là ");
			System.out.println(s);
			read.close();
		} catch (IOException e) {
			System.out.println("Không thể đọc File");
		}
	}
	public void ShowSV(){
		for (int i = 0; i < arr.size(); i++) {
			System.out.println(arr.get(i).toString());
		}
	}

}
